from main_form import Ui_Form
from PyQt5.QtWidgets import QTableWidgetItem
from PyQt5.QtWidgets import QHeaderView
from PyQt5.QtGui import QColor
import subprocess
import time
import parse_usb
import gzip

class MainFormSlots(Ui_Form):

	def __init__(self):
		self.q = 1
		# self.changeSelectedJournal
		# self.monitorControlUpdate
		# self.avCheck

	# метод, выводящий данные выбранного журнала
	def changeSelectedJournal(self):

		# Какой журнал выбран
		index = self.journalsTree.currentIndex()
		data = self.journalsTree.model().data(index)
		
		# очищается таблица
		self.journalText.setRowCount(0)
		self.journalText.setColumnCount(0)
		lines = ""

		# генерируем заготовку требуемой таблицы, читаем данные из нужного журнала, прописываем их:		
		# сами логи (точнее, их названия) в дерево загоняются в файле main.py
		if data != 'Системные' and data != 'Astra Linux' and data != 'Другие' and data != 'USB':
			parent = self.journalsTree.currentItem().parent().text(0)
			self.journalsTree.setHeaderLabel(parent)
			# Откуда читать журналы? Системные в /var/log, журналы Астры в /var/log/ald/
			if parent == 'Системные':
				if data[-2:] == 'gz':
					with gzip.open('/var/log/'+data, 'r') as f:
						lines = []
						for line in f.readlines():
							lines.append(line.decode('utf-8'))						
				else:						
					with open('/var/log/'+data, 'r') as f:
						lines = f.readlines()
				headers = ["Дата", "Время", "Компьютер", "Событие"]
				self.journalText.setColumnCount(4)
				for i in range(0, 4):
					self.journalText.setHorizontalHeaderItem(i, QTableWidgetItem(headers[i]))
				# из-за формата журнала некоторые строки приходится объединять
				for line in lines:
					line = line.split(" ", 4)
					self.journalText.insertRow(0)
					self.journalText.setItem(0, 0, QTableWidgetItem(line[0]+" "+line[1]))
					for i in range(1, 4):
						self.journalText.setItem(0, i, QTableWidgetItem(line[i+1]))
			# обработка журналов Астры
			elif parent == 'Astra Linux':
				with open('/var/log/ald/'+data) as f:
					lines = f.readlines()
				if data == "audit.log" or data == "aldlog.log":
					headers = ["Дата", "Время", "[s]", "IP:порт", "Инициатор", "Действие", "Событие"]
					self.journalText.setColumnCount(7)
					for i in range(0, 7):
						self.journalText.setHorizontalHeaderItem(i, QTableWidgetItem(headers[i]))
					for line in lines:
						line = line.split(" ", 8)
						self.journalText.insertRow(0)
						self.journalText.setItem(0, 0, QTableWidgetItem(line[0][1:]))
						self.journalText.setItem(0, 1, QTableWidgetItem(line[1][0:-5]))
						self.journalText.setItem(0, 2, QTableWidgetItem(line[2]))
						self.journalText.setItem(0, 3, QTableWidgetItem(line[3][4:-1]))
						self.journalText.setItem(0, 4, QTableWidgetItem(line[4][0:-1]))
						self.journalText.setItem(0, 5, QTableWidgetItem(line[5] + " " + line[6]))
						self.journalText.setItem(0, 6, QTableWidgetItem(line[7]))
				else:
					# из-за особенностей журнала вывод начинается сверху вниз (в отличие от системных журналов)
					sessionsCounter = 0
					subCounter = 0					
					headers  = ["Статус сессии","Дата", "Время", "Process[PID]", "[n]", "Событие"]
					self.journalText.setColumnCount(6)
					for i in range(0, 6):
						self.journalText.setHorizontalHeaderItem(i, QTableWidgetItem(headers[i]))
					for line in lines:						
						line = line.split(" ", 5)
						if line[0][0] == "-":							
							rowC = self.journalText.rowCount()
							self.journalText.insertRow(rowC)
							sessionStatus = "stop"
							if line[4] == "{":
								sessionStatus = "start"
								sessionsCounter += 1
							else:
								sessionStatus = "stop"
							self.journalText.setItem(rowC, 0, QTableWidgetItem("Session " + sessionStatus + " at " + line[1] + " " + line[2]))
							self.journalText.setVerticalHeaderItem(rowC, QTableWidgetItem(str(sessionsCounter)))
						elif line[0][0] == '(' or line[0] == "Не" or line[0] == "\n":
							pass
						else:							
							rowC = self.journalText.rowCount()
							self.journalText.insertRow(rowC)
							subCounter += 1
							self.journalText.setVerticalHeaderItem(rowC, QTableWidgetItem(str(sessionsCounter) + "." + str(subCounter)))
							self.journalText.setItem(rowC, 1, QTableWidgetItem(line[1] + " " + line[0]))
							for i in range(2, 6):
								self.journalText.setItem(rowC, i, QTableWidgetItem(line[i]))
					self.journalText.scrollToBottom()
			# заполнение логов для событий USB
			elif parent == 'USB':
				#какие заголовки столбцов будут:				
				headers = ["Дата", "Время", "Порт", "Имя", "Производитель", "ID"]
				self.journalText.setColumnCount(6)
				for i in range(0, 6):
					self.journalText.setHorizontalHeaderItem(i, QTableWidgetItem(headers[i]))
				# путь, где лежат логи. по умолчанию парсится syslog
				usb_log = '/var/log/'
				#чтение самих логов идёт через дополнительную функцию парсинга syslog
				#см. файл parse_usb.py
				logs = parse_usb.parseUSB(usb_log+data)
				for event in logs:
					if event:
						self.journalText.insertRow(0)
						for col in event[2:]:
							self.journalText.setItem(0, event.index(col)-2, QTableWidgetItem(col))
							if event[1] == 1:
								self.journalText.item(0, event.index(col)-2).setBackground(QColor(0,200,0,200))
							elif event[1] == 0:
								self.journalText.item(0, event.index(col)-2).setBackground(QColor(200,0,0,100))
			self.journalText.resizeColumnsToContents()

	# Метод, отвечающий за проверку наличия антивирусного ПО
	def __avCheck(self):
		res = 0
		c = [line.decode('cp866', 'ignore') for line in subprocess.Popen(['pidof', 'drwebd'], stdout=subprocess.PIPE).stdout.readlines()]
		if len(c) == 0:
			c = [line.decode('cp866', 'ignore') for line in subprocess.Popen(['pidof', 'kesd'], stdout=subprocess.PIPE).stdout.readlines()]
			if len(c) == 0:
				return 0
			else:
				return 1
		else:			
			return 1

	# Метод, отвечающий за анализ состояния системы Защищённой Среды
	def __clWSCheck(self):
		c = 0
		path = '/etc/digsig/digsig_initramfs.conf'
		try:
			f = open(path)
		except IOError as e:
			return 0
		else:
			with open(path) as f:
				lines = f.readlines()
				for line in lines:
					if line == 'DIGSIG_LOAD_KEYS=1\n':
						c += 1
					elif line == 'DIGSIG_ENFORCE=1\n':
						c +=1
		return c

	# Метод, отвечающий за проверку активности Контроля Целостности
	def __kcCheck(self):
		updd = 0
		path = '/var/lib/afick/history'
		try:
			f = open(path)
		except IOError as e:
			return 0
		else:
			with open(path) as f:
				lines = f.readlines()
				for line in lines:
					line = line.split(' ')
					if line[3] == 'init':
						updd = 'Инициализация от '+line[0]+' в '+line[1]
					elif updd != 0 and line[3] == 'update':
						updd = 'Последняя сверка от '+line[0]+' в '+line[1]
		return updd

	def monitorControlUpdate(self):
		av = self.__avCheck()
		kc = self.__kcCheck()
		cl = self.__clWSCheck()
		self.recomendText.setText("")
		i = 0

		# Проверка наличия антивирусного ПО
		if av:
			self.avStatusLabel.	setStyleSheet("QLabel { background-color : green}")
			self.avStatusLabel.setText("ВКЛЮЧЕНА")
		else:
			self.avStatusLabel.	setStyleSheet("QLabel { background-color : red}")
			self.avStatusLabel.setText("ВЫКЛЮЧЕНА")
			i+=1
			self.recomendText.append(str(i)+")Рекомендуется включить антивирусное ПО. Рекомендуемые антивирусные программы:<br> Dr.Web или Kaspersky Antivirus")

		# Проверка учёта контроля целостности
		if kc == 0:
			self.integControlLabel.	setStyleSheet("QLabel { background-color : red}")
			self.integControlLabel.setText("ВЫКЛЮЧЕНА")
			i+=1
			self.recomendText.append(str(i)+") Рекомендуется настроить контроль целостности")
		else:
			self.integControlLabel.	setStyleSheet("QLabel { background-color : green}")
			self.integControlLabel.setText(kc)

		# Проверка активности защищённой среды
		if cl == 2:
			self.clWSLabel.	setStyleSheet("QLabel { background-color : green}")
			self.clWSLabel.setText("ВКЛЮЧЕНА")
		elif cl == 1:
			self.clWSLabel.	setStyleSheet("QLabel { background-color : orange}")
			self.clWSLabel.setText("ПРОВЕРКА ПОДПИСИ")
			i+=1
			self.recomendText.append(str(i)+") Включен режим только проверки подписи. Рекомендуется включить полный контроль приложений")
		else:
			self.clWSLabel.	setStyleSheet("QLabel { background-color : red}")
			self.clWSLabel.setText("ВЫКЛЮЧЕНА")
			i+=1
			self.recomendText.append(str(i)+") Защищённая программная среда не определена! Рекомендуется настроить")

